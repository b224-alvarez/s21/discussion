// alert("Hello World!");

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

console.log(studentNumberA);
console.log(studentNumberB);
console.log(studentNumberC);
console.log(studentNumberD);
console.log(studentNumberE);

let studentID = [
  "2020-1923",
  "2020-1924",
  "2020-1925",
  "2020-1926",
  "2020-1927",
];
console.log(studentID);

/*
	ARRAYS
		- used to store multiple related values in a single variable.
		- declared using the square bracket ([]); This declaration is also known as "Array Literals";

		Syntax:
			let/const arrayName = [elementA, elementB, ... elementN];
*/

let grades = [95.6, 98.3, 87.6, 90.4];
let computerBrands = [
  "Acer",
  "Asus",
  "Lenovo",
  "Neo",
  "Redfox",
  "Gateway",
  "Toshiba",
  "Fujitsu",
];

let mixedArr = [12, "Asus", null, undefined, {}]; //not recommended.
console.log(mixedArr);

let myTasks = ["drink html", "eat javascript", "inhale css", "bake sass"];

console.log(myTasks);

// We can also store values of separate variables in an array
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];
let citySample = ["Tokyo", "Manila", "Jakarta"];
console.log(citySample);
console.log(cities);
console.log(city1 == "Tokyo");
console.log(city2 == "Manila");
console.log(city3 == "Jakarta");
// console.log(cities == citySample);// supposedly result to true.

// Array Length Property
console.log(myTasks.length); //4
console.log(cities.length); //3

let blankArr = [];
console.log(blankArr.length);

// Length property of a string
let fullName = "Rupert Ramos";
console.log(fullName.length); //12

// Shorten the array
myTasks.length = myTasks.length - 1;
console.log(myTasks.length);
console.log(myTasks);

cities.length--;
console.log(cities.length);
console.log(cities);

// Can we delete a character in a string using .length property?
console.log(fullName.length);
fullName.length = fullName.length - 1;
console.log(fullName.length);

fullName.length--;
console.log(fullName);
// Answer: No, we can only delete or add using ".length" property in an array.

// Lengthen the array

let theBeatles = ["John", "Paul", "Ringo", "George"];
console.log(theBeatles.length);
theBeatles.length++;
console.log(theBeatles.length);
console.log(theBeatles);

// Assigning a value to a specific array index
// arrayName[i] = "new value";
theBeatles[4] = "Ms. Chris";
console.log(theBeatles);

/*
	Accessing Elements of an Array

	Syntax:
		arrayName[index];

*/

console.log(grades[0]);
console.log(computerBrands[3]);

/*console.log(grades[20]);
console.log(grades);
grades[20] = 90.7;
console.log(grades[20]);
console.log(grades);
- Although this is possible, still this is not a good practice.
*/

//0      1        2         3        4
let lakerLegends = ["Kobe", "Shaq", "LeBron", "Magic", "Kareem"];
console.log(lakerLegends[1]);
console.log(lakerLegends[4]);

// Can we save array items in another variable?
let currentLaker = lakerLegends[2];
console.log(currentLaker);

//Reassigning value/s in array
console.log("Array before reassignment:");
console.log(lakerLegends);
lakerLegends[2] = "Pau Gasol";
console.log("Array after reassignment:");
console.log(lakerLegends);

// Accessing the last element of an array

let bullsLegend = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullsLegend.length - 1;
console.log(bullsLegend[lastElementIndex]);

console.log(bullsLegend[bullsLegend.length - 1]);

// Addin items into the Array
let newArr = [];
console.log(newArr);
console.log(newArr[0]);
newArr[0] = "Jenny";
console.log(newArr);
newArr[1] = "Jisoo";
console.log(newArr);

// What is we want to add an element at the end of our array?
console.log(newArr.length);
newArr[newArr.length] = "Lisa";
console.log(newArr);
console.log(newArr.length);
newArr[newArr.length] = "Rose";
console.log(newArr);
console.log(newArr.length);

/*
	Mini Activity: (10 minutes)

	Part 1: Adding a value at the end of an array
		- Create a function which is able to receive a single argument and add the input at the end of the superheroes array.
		- Invoke and add an argument to be passed to the function.
		- Log the superheroes array in the console.

	Part 2:Retrieving an element using a function.
		- Create a function which is able to receive an index number as a single argument.
		- Return the element/item accessed by the index.
		- Create a global variable named heroFound and store/pass the value returned by the function.
		- Log the heroFound variable in the console.

*/

// Solution:

// Part 1:
let superHeroes = ["Iron Man", "Spiderman", "Captain America"];

function marvelHeroes(heroName) {
  superHeroes[superHeroes.length] = heroName;
}

marvelHeroes("Thor");
console.log(superHeroes);
marvelHeroes("Wanda");
console.log(superHeroes);

// Part 2:

function getHeroByIndex(index) {
  return superHeroes[index];
}

let heroFound = getHeroByIndex(2);
console.log(heroFound);

// Loops over an Array

for (let index = 0; index < newArr.length; index++) {
  console.log(newArr[index]);
}

let numArr = [5, 12, 30, 46, 50, 88];

for (let index = 0; index < numArr.length; index++) {
  if (numArr[index] % 5 === 0) {
    console.log(numArr[index] + " is divisible by 5.");
  } else {
    console.log(numArr[index] + " is not divisible by 5.");
  }
}

console.log("");

// Multidimensional Array

let chessBoard = [
  ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"], //0
  ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"], //1
  ["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"], //2
  ["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"], //3
  ["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"], //4
  ["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"], //5
  ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"], //6
  ["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"], //7
];

console.log(chessBoard);
console.log(chessBoard[1][4]);
console.log("Pawn moves to: " + chessBoard[7][4]);
